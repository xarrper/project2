package com.xarrper.olga.progect2.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xarrper.olga.progect2.R;

public class OneFragment extends Fragment {

    private Callbacks callbacks;

    public interface Callbacks {
        void onCrimeSelected();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbacks.onCrimeSelected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.one_fragment, parent, false);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        callbacks = (Callbacks)activity;
    }
    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

}
