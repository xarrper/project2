package com.xarrper.olga.progect2.fragment;

import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.LayoutInflaterCompat;
import android.util.Log;
import android.view.View;

import com.xarrper.olga.progect2.R;
import com.xarrper.olga.progect2.activity.MainActivity;

public class TestDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public static final String EXTRA_DATA =
            "com.xarrper.olga.progect2.fragment.data";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = (String) getArguments().getSerializable(EXTRA_DATA);

        View v = getActivity().getLayoutInflater()
                .inflate(R.layout.dialog, null);

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(title)
                .setPositiveButton(android.R.string.ok, this)
                .create();
    }

    public static TestDialogFragment newInstance(String data) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_DATA, data);
        TestDialogFragment fragment = new TestDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(DialogInterface dialog, int position) {
        Log.d("TestDialogFragment", "onClick");
        MainActivity callingActivity = (MainActivity) getActivity();
        callingActivity.onResultForDialog("Результат от диалога");
        dialog.dismiss();
    }


}
