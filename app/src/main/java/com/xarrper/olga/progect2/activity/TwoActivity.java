package com.xarrper.olga.progect2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.xarrper.olga.progect2.R;

public class TwoActivity extends AppCompatActivity {

    public static final String KEY_PARAMETER = "com.xarrper.olga.progect2.key_parameter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(this, "Вторая акивность", Toast.LENGTH_LONG).show();

        String parameter = getIntent().getStringExtra(TwoActivity.KEY_PARAMETER);
        Toast.makeText(this, parameter, Toast.LENGTH_LONG).show();

        Button b = (Button)findViewById(R.id.twoActivity);
        b.setText("Вернутся назад");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra(KEY_PARAMETER, "TwoActivity");
                setResult(RESULT_OK, i);
                TwoActivity.this.finish();
            }
        });


    }

}
