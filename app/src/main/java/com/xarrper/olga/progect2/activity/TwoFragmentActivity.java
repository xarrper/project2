package com.xarrper.olga.progect2.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.xarrper.olga.progect2.R;
import com.xarrper.olga.progect2.fragment.OneFragment;
import com.xarrper.olga.progect2.fragment.TwoFragment;

public class TwoFragmentActivity extends SingleFragmentActivity implements OneFragment.Callbacks {

    @Override
    protected Fragment createFragment() {
        return new OneFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_two_fragment;
    }

    @Override
    public void onCrimeSelected() {
        if (findViewById(R.id.twoFragmentContainer) != null) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment oldDetail = fm.findFragmentById(R.id.twoFragmentContainer);
            if (oldDetail != null) {
                ft.remove(oldDetail);
            }
            ft.add(R.id.twoFragmentContainer, new TwoFragment());
            ft.commit();
        }
    }
}
