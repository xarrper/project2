package com.xarrper.olga.progect2.activity;

import android.support.v4.app.Fragment;

import com.xarrper.olga.progect2.fragment.ExampleListFragment;

public class ListActivity extends SingleFragmentActivity {


    @Override
    protected Fragment createFragment() {
        return new ExampleListFragment();
    }
}
