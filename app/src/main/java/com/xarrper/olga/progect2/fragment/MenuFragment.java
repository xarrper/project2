package com.xarrper.olga.progect2.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.xarrper.olga.progect2.R;

public class MenuFragment extends ListFragment {

    final String[] catNames = new String[] {
            "Рыжик", "Барсик", "Мурзик", "Мурка", "Васька",
            "Томасина", "Кристина", "Пушок", "Дымка", "Кузя",
            "Китти", "Масяня", "Симба"
    };

    private ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, catNames);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list, parent, false);
        ListView listView = (ListView)v.findViewById(android.R.id.list);

        listView.setAdapter(adapter);

//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            registerForContextMenu(listView);
//        } else {
//            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
//            listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
//                public void onItemCheckedStateChanged(ActionMode mode, int position,
//                                                      long id, boolean checked) {
//                }
//                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//                    MenuInflater inflater = mode.getMenuInflater();
//                    inflater.inflate(R.menu.context, menu);
//                    return true;
//                }
//
//                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//                    return false;
//                }
//
//                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//                    switch (item.getItemId()) {
//                        case R.id.menu_item_delete_crime:
//                            for (int i = adapter.getCount() - 1; i >= 0; i--) {
//                                if (getListView().isItemChecked(i)) {
//                                    Toast.makeText(getContext(), adapter.getItem(i), Toast.LENGTH_LONG).show();
//                                }
//                            }
//                            mode.finish();
//                            return true;
//                        default:
//                            return false;
//                    }
//                }
//                public void onDestroyActionMode(ActionMode mode) {
//                    // Метод является обязательным, но не используется
//                    // в этой реализации
//                }
//            });
//        }

        return v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.arrow:
                Toast.makeText(getContext(), R.string.arrow, Toast.LENGTH_LONG).show();
                return true;
            case R.id.codderz:
                Toast.makeText(getContext(), R.string.codderz, Toast.LENGTH_LONG).show();
                return true;
            case R.id.settings:
                Toast.makeText(getContext(), R.string.settings, Toast.LENGTH_LONG).show();
                return true;
            case R.id.view:
                Toast.makeText(getContext(), R.string.view, Toast.LENGTH_LONG).show();
                return true;
            case R.id.exit:
                Toast.makeText(getContext(), R.string.exit, Toast.LENGTH_LONG).show();
                return true;
            case android.R.id.home:
                Toast.makeText(getContext(), "назад", Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.action_bar, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int position = info.position;

        switch (item.getItemId()) {
            case R.id.menu_item_delete_crime:
                Toast.makeText(getContext(),  adapter.getItem(position), Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.context, menu);
    }

}
