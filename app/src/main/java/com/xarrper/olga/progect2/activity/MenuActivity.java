package com.xarrper.olga.progect2.activity;

import android.support.v4.app.Fragment;

import com.xarrper.olga.progect2.fragment.MenuFragment;

public class MenuActivity extends SingleFragmentActivity {

    protected Fragment createFragment() {
        return new MenuFragment();
    }
}
