package com.xarrper.olga.progect2.activity;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.xarrper.olga.progect2.R;
import com.xarrper.olga.progect2.fragment.TestDialogFragment;

public class MainActivity extends AppCompatActivity {

    private static final int TWO_ACTIVITY = 0;
    private static final String DIALOG_DATA = "data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button)findViewById(R.id.twoActivity);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, TwoActivity.class);
                //startActivity(i);
                i.putExtra(TwoActivity.KEY_PARAMETER, "MainActivity");
                startActivityForResult(i, TWO_ACTIVITY);
            }
        });

        Button b1 = (Button)findViewById(R.id.fragment);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HostActivity.class));
            }
        });

        Button b2 = (Button)findViewById(R.id.menu);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MenuActivity.class));
            }
        });

        Button b3 = (Button)findViewById(R.id.intent);
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, "Заголовок");
                i.putExtra(Intent.EXTRA_SUBJECT, "Текст");
                startActivity(i);
            }
        });

        Button b4 = (Button)findViewById(R.id.dialog);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                TestDialogFragment dialog = TestDialogFragment
                        .newInstance("Привет");
                dialog.show(fm, DIALOG_DATA);
            }
        });

        Button b5 = (Button)findViewById(R.id.listInformation);
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ListActivity.class));
            }
        });

        Button b6 = (Button)findViewById(R.id.twoFragment);
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, TwoFragmentActivity.class));
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Toast.makeText(this, (String)data.getSerializableExtra(TwoActivity.KEY_PARAMETER), Toast.LENGTH_LONG).show();
    }

    public void onResultForDialog(String selectedValue) {
        Toast.makeText(this, selectedValue, Toast.LENGTH_LONG).show();
    }

}
