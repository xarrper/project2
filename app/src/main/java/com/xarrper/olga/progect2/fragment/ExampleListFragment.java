package com.xarrper.olga.progect2.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.xarrper.olga.progect2.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExampleListFragment extends ListFragment {

    final ArrayList<String> catNames = new ArrayList<String>();
//    (
//            "Рыжик", "Барсик", "Мурзик", "Мурка", "Васька",
//            "Томасина", "Кристина", "Пушок", "Дымка", "Кузя",
//            "Китти", "Масяня", "Симба"
//    );

    private ArrayAdapter<String> adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, catNames);
        setListAdapter(adapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_list, parent, false);
        ListView listView = (ListView) v.findViewById(android.R.id.list);

//        listView.setAdapter(adapter);

        Button change = (Button)v.findViewById(R.id.addElement);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                catNames.add("Пес");
                adapter.notifyDataSetChanged();
//                adapter.add("Кот");

            }
        });

        return v;
    }

//    private class PointListAdapter extends ArrayAdapter<Map<String, String>> {
//
//        public PointListAdapter(ArrayList<Map<String, String>> pointList) {
//            super(getActivity(), 0, pointList);
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//
//            if (convertView == null) {
//                convertView = getActivity().getLayoutInflater().inflate(R.layout.element_list, null);
//            }
//
//            final Map<String, String> p = getItem(position);
//            TextView nameTextView = (TextView)convertView.findViewById(R.id.title);
//            nameTextView.setText(p.);
//
//
//
//            return convertView;
//        }
//    }


}
